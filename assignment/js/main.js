// "hello_text"idのテキスト変更
document.getElementById("hello_text").textContent = "First JavaScript";

// キーボードイベントを監視
document.addEventListener("keydown", onKeyDown);

// 一定時間で繰り返す処理
let count = 0;

// ゲーム盤
let cells;

// 落下中フラグ
let isFalling = false;

// 落下中ブロック番号
let fallingBlockNum = 0;

// ブロックのパターン
let blocks = {
  i: {
    class: "i",
    pattern: [[1, 1, 1, 1]],
  },
  o: {
    class: "o",
    pattern: [
      [1, 1],
      [1, 1],
    ],
  },
  t: {
    class: "t",
    pattern: [
      [0, 1, 0],
      [1, 1, 1],
    ],
  },
  s: {
    class: "s",
    pattern: [
      [0, 1, 1],
      [1, 1, 0],
    ],
  },
  z: {
    class: "z",
    pattern: [
      [1, 1, 0],
      [0, 1, 1],
    ],
  },
  j: {
    class: "j",
    pattern: [
      [1, 0, 0],
      [1, 1, 1],
    ],
  },
  l: {
    class: "l",
    pattern: [
      [0, 0, 1],
      [1, 1, 1],
    ],
  },
};

// キーボード入力によってそれぞれの関数を呼び出す
function onKeyDown(event) {
  if (event.keyCode === 37) {
    moveLeft();
  } else if (event.keyCode === 39) {
    moveRight();
  }
}

// ゲーム盤を読み込む
loadTable();
setInterval(function () {
  // 何回目かを数えるために変数countを1ずつ増やす
  count++;
  document.getElementById("hello_text").textContent =
    "First JavaScript - " + count;

  // 落下中のブロックがあるか確認
  if (hasFallingBlock()) {
    // ブロックを落とす
    fallBlocks();
  } else {
    // 揃っている行を削除
    deleteRow();
    // ブロックが積み上がりきっていないか判定
    isGameover();
    // ランダムにブロックを生成
    generateBlock();
  }
}, 100);

function loadTable() {
  // タグ名より表要素取得
  let td_array = document.getElementsByTagName("td");

  // 2次元座標付け
  cells = [];
  let index = 0;
  for (let row = 0; row < 20; row++) {
    // 配列要素をさらに配列にする（2次元配列にする）
    cells[row] = [];
    for (let col = 0; col < 10; col++) {
      cells[row][col] = td_array[index];
      index++;
    }
  }
}

function fallBlocks() {
  // 一番下の行にブロックがあれば落下中フラグを変更
  for (let i = 0; i < 10; i++) {
    if (cells[19][i].blockNum === fallingBlockNum) {
      isFalling = false;
      return;
    }
  }

  // 下の烈に別のブロックがないか
  for (let row = 18; row >= 0; row--) {
    for (let col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        if (
          cells[row + 1][col].className !== "" &&
          cells[row + 1][col].blockNum !== fallingBlockNum
        ) {
          isFalling = false;
          return;
        }
      }
    }
  }

  // 上のクラスを下に付与する
  for (let row = 18; row >= 0; row--) {
    for (let col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        // 下の列にクラスを入れる
        cells[row + 1][col].className = cells[row][col].className;
        cells[row + 1][col].blockNum = cells[row][col].blockNum;
        // 上の列を空にする
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}

function hasFallingBlock() {
  return isFalling;
}

function isGameover() {
  // ゲームオーバー判定
  for (let row = 0; row < 2; row++) {
    for (let col = 0; col < 10; col++) {
      if (cells[row][col].className !== "") {
        // アラート表示
        alert("game over");
        resetGame();
      }
    }
  }
}

function deleteRow() {
  for (let row = 19; row >= 0; row--) {
    let canDelete = true;
    // 削除可能か判別
    for (let col = 0; col < 10; col++) {
      if (cells[row][col].className === "") {
        canDelete = false;
      }
    }
    if (canDelete) {
      // 1行削除
      for (let i = 0; i < 10; i++) {
        cells[row][i].className = "";
        cells[row][i].blockNum = null;
      }
      // 行を上から下へ詰める
      for (let i = row - 1; i >= 0; i--) {
        for (let j = 0; j < 10; j++) {
          cells[i + 1][j].className = cells[i][j].className;
          cells[i + 1][j].blockNum = cells[i][j].blockNum;
          cells[i][j].className = "";
          cells[i][j].blockNum = null;
        }
      }
    }
  }
}

function generateBlock() {
  // ブロックパターンからランダムにパターンを選択
  // keys = ["i", "o", ...]
  let keys = Object.keys(blocks);
  // nextBlockKey = "i"
  let nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
  // nextBlock = { class: "i", pattern: [[1,1,1,1]] }
  let nextBlock = blocks[nextBlockKey];
  let nextFalllingBlockNum = fallingBlockNum + 1;

  // 選択したパターンのブロックを配置
  // pattern = [[1,1,1,1]]
  let pattern = nextBlock.pattern;
  for (let row = 0; row < pattern.length; row++) {
    for (let col = 0; col < pattern[row].length; col++) {
      if (pattern[row][col]) {
        // element.classNameでクラス名を取得
        cells[row][col + 3].className = nextBlock.class;
        // blockNumという変数フィールドを作成しnextFallingBlockNumを保存
        cells[row][col + 3].blockNum = nextFalllingBlockNum;
      }
    }
  }

  // 落下中フラグを変更
  isFalling = true;
  fallingBlockNum = nextFalllingBlockNum;
}

function moveRight() {
  let isExist = false;

  // 落下中のクラスを取得し落下中ブロック番号のブロックを右へ移動
  for (let row = 0; row < 20; row++) {
    // 右端に落下中のブロックが存在するか
    if (
      cells[row][9].className !== "" &&
      cells[row][9].blockNum === fallingBlockNum
    ) {
      isExist = true;
    }
  }

  if (!isExist) {
    for (let i = 0; i < 20; i++) {
      for (let j = 8; j >= 0; j--) {
        if (cells[i][j].blockNum === fallingBlockNum) {
          cells[i][j + 1].className = cells[i][j].className;
          cells[i][j + 1].blockNum = cells[i][j].blockNum;
          cells[i][j].className = "";
          cells[i][j].blockNum = null;
        }
      }
    }
  }
}


function moveLeft() {
  let isExist = false;

  // 落下中のクラスを取得し落下中ブロック番号のブロックを左へ移動
  for (let row = 0; row < 20; row++) {
    // 左端に落下中のブロックが存在するか
    if (
      cells[row][0].className !== "" &&
      cells[row][0].blockNum === fallingBlockNum
    ) {
      isExist = true;
    }
  }

  if (!isExist) {
    for (let i = 0; i < 20; i++) {
      // １マス右にずらす
      for (let j = 1; j < 10; j++) {
        if (cells[i][j].blockNum === fallingBlockNum) {
          cells[i][j - 1].className = cells[i][j].className;
          cells[i][j - 1].blockNum = cells[i][j].blockNum;
          cells[i][j].className = "";
          cells[i][j].blockNum = null;
        }
      }
    }
  }
}

function resetGame() {
  // ゲームの初期化
  count = 0;
  isFalling = false;
  fallingBlockNum = 0;

  // 盤面のリセット
  for (let row = 0; row < 20; row++) {
    for (let col = 0; col < 10; col++) {
      cells[row][col].className = "";
      cells[row][col].blockNum = null;
    }
  }
}
